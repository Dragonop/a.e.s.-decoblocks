local function register_tunnel_block(n)

  minetest.register_node("decoblocks:blockleague_tunnel_" .. n, {
    description = "[BL] Tunnel " .. n,
    tiles = {
      "decoblocks_blockleague_tunnel_" .. n .. ".png",
      "decoblocks_concrete_blue.png"
    },
    paramtype2 = "facedir",
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults()
  })

  local n2 = n + 3

  minetest.register_node("decoblocks:blockleague_tunnel_" .. n2, {
    description = "[BL] Tunnel " .. n2,
    drawtype = "nodebox",
    tiles = {
      "decoblocks_blockleague_tunnel_" .. n .. ".png",
      "decoblocks_concrete_blue.png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    paramtype2 = "facedir",
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults()
  })

end



local function register_hyperium_block(n)

	minetest.register_node("decoblocks:blockleague_hyperium_" .. n, {
    description = "[BL] Hyperium " .. n,
    drawtype = "nodebox",
    tiles = {
      "decoblocks_blockleague_hyperium_" .. n .. ".png",
      "decoblocks_concrete_black.png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
	paramtype = "light",
    paramtype2 = "facedir",
	light_source = default.LIGHT_MAX/2,
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults()
  })

end



register_tunnel_block(1)
register_tunnel_block(2)
register_tunnel_block(3)

register_hyperium_block(1)
register_hyperium_block(2)
