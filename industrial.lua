local S = minetest.get_translator("decoblocks")



minetest.register_node("decoblocks:chainlink_fence", {
    description = S("Chain-link fence"),
    drawtype = "signlike",
    tiles = {"decoblocks_chainlink_fence.png"},
    inventory_image = "decoblocks_chainlink_fence.png",
    wield_image = "decoblocks_chainlink_fence.png",
    paramtype = "light",
    paramtype2 = "wallmounted",
    sunlight_propagates = true,
    selection_box = {type = "wallmounted"},
    node_box = {type = "wallmounted",},
    groups = {cracky = 3}
})

minetest.register_node("decoblocks:flyer", {
  description = S("Flyer"),
  tiles = {
    "decoblocks_flyer.png",
    "blank.png",
    "blank.png",
    "blank.png",
    "blank.png",
    "blank.png"
  },
  drawtype = "nodebox",
  paramtype = "light",
  paramtype2 = "facedir",
  node_box = {
    type = "fixed",
    fixed = {
      {-0.5, -0.5, -0.5, 0.5, -0.49, 0.5}
    }
  },
  groups = {oddly_breakable_by_hand = 3}
})


minetest.register_alias("chainlink_fence", "decoblocks:chainlink_fence")
minetest.register_alias("flyer", "decoblocks:flyer")
