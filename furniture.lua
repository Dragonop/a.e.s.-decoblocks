local S = minetest.get_translator("decoblocks")



minetest.register_node("decoblocks:barrel", {
  description = S("Barrel"),
  tiles = {
    "decoblocks_barrel_top.png",
    "decoblocks_barrel_top.png",
    "decoblocks_barrel_side.png"
  },
  paramtype2 = "facedir",
  groups = {choppy=3},
  sounds = default.node_sound_wood_defaults(),
})



minetest.register_node("decoblocks:hay", {
  description = S("Hay"),
  tiles = {
    "decoblocks_hay_top.png",
    "decoblocks_hay_top.png",
    "decoblocks_hay_side.png"
  },
  paramtype2 = "facedir",
  groups = {oddly_breakable_by_hand = 3},
  sounds = default.node_sound_leaves_defaults(),
})

minetest.register_alias("hay", "decoblocks:hay")
minetest.register_alias("barrel", "decoblocks:barrel")










minetest.register_node("decoblocks:vase", {
  description = "Vase",
	tiles = {
		"decoblocks_coconut_side.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.1875, -0.5, -0.1875, 0.1875, -0.375, 0.1875}, -- NodeBox1
			{-0.1875, -0.375, -0.125, -0.125, -0.125, 0.1875}, -- NodeBox4
			{0.125, -0.375, -0.1875, 0.1875, -0.125, 0.125}, -- NodeBox6
			{-0.1875, -0.375, -0.1875, 0.125, -0.125, -0.125}, -- NodeBox7
			{-0.125, -0.375, 0.125, 0.1875, -0.125, 0.1875}, -- NodeBox8
		}
	},
  groups = {oddly_breakable_by_hand = 2}
})
