local S = minetest.get_translator("decoblocks")



local function register_concrete(name, desc)

  name = "concrete_" .. name
  desc = S(desc)

  minetest.register_node("decoblocks:" .. name, {
  	description = desc,
  	tiles = {
  		"decoblocks_" .. name .. ".png",
  	},
  	groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults()
  })

  minetest.register_node("decoblocks:" .. name .. "_slab", {
  	description = desc .. " " .. S("(slab)"),
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
  	tiles = {
  		"decoblocks_" .. name .. ".png",
  	},
    node_box = {
  		type = "fixed",
  		fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
  	groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults(),

    on_place = function(itemstack, placer, pointed_thing)
      place_and_rotate(itemstack, placer, pointed_thing)
		end,
  })

  minetest.register_alias(name, "decoblocks:" .. name)
  minetest.register_alias(name .. "_slab", "decoblocks:" .. name .. "_slab")

end

register_concrete("blue", "Blue concrete")
register_concrete("red", "Red concrete")
register_concrete("grey", "Grey concrete")
register_concrete("light_grey", "Light grey concrete")
register_concrete("dark_grey", "Dark grey concrete")
register_concrete("black", "Black concrete")





function place_and_rotate(itemstack, placer, pointed_thing)
  local p0 = pointed_thing.under
  local p1 = pointed_thing.above
  local param2 = 0

  if placer then
    local placer_pos = placer:get_pos()
    if placer_pos then
      param2 = minetest.dir_to_facedir(vector.subtract(p1, placer_pos))
    end

    local finepos = minetest.pointed_thing_to_face_pos(placer, pointed_thing)
    local fpos = finepos.y % 1

    if p0.y - 1 == p1.y or (fpos > 0 and fpos < 0.5)
        or (fpos < -0.5 and fpos > -0.999999999) then
      param2 = param2 + 20
      if param2 == 21 then
        param2 = 23
      elseif param2 == 23 then
        param2 = 21
      end
    end
  end

  return minetest.item_place(itemstack, placer, pointed_thing, param2)
end
