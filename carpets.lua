local S = minetest.get_translator("decoblocks")



local function register_carpet(name, desc, texture)

  local short_name = "carpet_" .. name
  desc = S(desc)

  minetest.register_node("decoblocks:" .. short_name, {
  	description = desc,
  	tiles = {
  		"wool_" .. name .. ".png"
  	},
  	drawtype = "nodebox",
  	paramtype = "light",
  	node_box = {
  		type = "fixed",
  		fixed = {
  			{-0.5, -0.5, -0.5, 0.5, -0.4375, 0.5}
  		}
  	},
  	groups = {oddly_breakable_by_hand = 3}
  })

  minetest.register_alias(short_name, "decoblocks:" .. short_name)

end

register_carpet("black", "Black carpet")
register_carpet("blue", "Blue carpet")
register_carpet("brown", "Brown carpet")
register_carpet("cyan", "Cyan carpet")
register_carpet("dark_green", "Dark green carpet")
register_carpet("dark_grey", "Dark grey carpet")
register_carpet("green", "Green carpet")
register_carpet("grey", "Grey carpet")
register_carpet("magenta", "Magenta carpet")
register_carpet("orange", "Orange carpet")
register_carpet("pink", "Pink carpet")
register_carpet("red", "Red carpet")
register_carpet("violet", "Violet carpet")
register_carpet("white", "White carpet")
register_carpet("yellow", "Yellow carpet")
