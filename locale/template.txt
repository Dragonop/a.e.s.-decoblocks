# version 0.1.0-dev
# author(s):
# reviewer(s):
# textdomain: decoblocks

# carpets.lua
Black carpet=
Blue carpet=
Brown carpet=
Cyan carpet=
Dark green carpet=
Dark grey carpet=
Green carpet=
Grey carpet=
Magenta carpet=
Orange carpet=
Pink carpet=
Red carpet=
Violet carpet=
White carpet=
Yellow carpet=

# concrete.lua
(slab)=
Blue concrete=
Red concrete=
Grey concrete=
Light grey concrete=
Dark grey concrete=
Black concrete=

#furniture.lua
Barrel=
Hay=

#industrial.lua
Chain-link fence=
Flyer=

#nature.lua
Beaten path=
Cobweb=
String=
